package com;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TripleTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@Test
	void testCheckTripplets1() {
		int[] array = { 3, 3, 5, 5, 5, 2, 3 };
		boolean actual = new UserMainCode().checkTripplets(array);
		assertEquals(true, actual);
	}

	@Test
	void testCheckTripplets2() {
		int[] array = { 5, 3, 5, 1, 5, 2, 3 };
		boolean actual = new UserMainCode().checkTripplets(array);
		assertEquals(false, actual);
	}

}
