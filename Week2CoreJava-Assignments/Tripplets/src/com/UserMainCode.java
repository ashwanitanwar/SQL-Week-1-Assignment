package com;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserMainCode {
	public static boolean checkTripplets(int[] array) {
		if (array.length == 0 || array.length == 1 || array.length == 2)
			return false;

		int counter = 1;
		int a = array[0];

		for (int i = 1; i < array.length; i++) {
			// System.out.println("counter:"+counter+"a:"+a);
			if (array[i] == a)
				counter++;
			else {
				if (counter >= 3)
					return true;
				else {
					counter = 1;
					a = array[i];
				}

			}
		}
		if(counter==3)
			return true;
		return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		int total = 0;
		System.out.println("Enter n: total no of elements followed by n numbers");
		try {
			total = scan.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Wrong input");
			System.exit(0);
		}

		int[] array = new int[total];
		try {
			for (int i = 0; i < total; i++) {
				array[i] = scan.nextInt();
			}
		} catch (InputMismatchException e) {
			System.out.println("Wrong inputs");
		}
		System.out.println(checkTripplets(array));
	}

}
