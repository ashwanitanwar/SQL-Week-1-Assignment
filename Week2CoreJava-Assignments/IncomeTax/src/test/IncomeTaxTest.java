package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.EmployeeBo;
import com.EmployeeVo;

class IncomeTaxTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@Test
	void testCalincomeTax() {
		double actual=new EmployeeBo().calincomeTax(new EmployeeVo(56, "ashwani", 1000000, 0));

		assertEquals(80000, actual);
	}

}
