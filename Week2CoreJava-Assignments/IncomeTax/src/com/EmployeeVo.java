package com;

public class EmployeeVo {

	int empid;
	String empname;
	double annual_income;
	double income_tax;

	public EmployeeVo(int empid, String empname, double annual_income, double income_tax) {
		this.empid = empid;
		this.empname = empname;
		this.annual_income = annual_income;
		this.income_tax = income_tax;
	}

	public int getEmpid() {
		return empid;
	}

	public void setEmpid(int empid) {
		this.empid = empid;
	}

	public String getEmpname() {
		return empname;
	}

	public void setEmpname(String empname) {
		this.empname = empname;
	}

	public double getAnnual_income() {
		return annual_income;
	}

	public void setAnnual_income(double annual_income) {
		this.annual_income = annual_income;
	}

	public double getIncome_tax() {
		return income_tax;
	}

	public void setIncome_tax(double income_tax) {
		this.income_tax = income_tax;
	}

	@Override
	public String toString() {
		return "[empid=" + empid + ", empname=" + empname + ", annual_income=" + annual_income + ", income_tax="
				+ income_tax + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(annual_income);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + empid;
		result = prime * result + ((empname == null) ? 0 : empname.hashCode());
		temp = Double.doubleToLongBits(income_tax);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeVo other = (EmployeeVo) obj;
		if (Double.doubleToLongBits(annual_income) != Double.doubleToLongBits(other.annual_income))
			return false;
		if (empid != other.empid)
			return false;
		if (empname == null) {
			if (other.empname != null)
				return false;
		} else if (!empname.equals(other.empname))
			return false;
		if (Double.doubleToLongBits(income_tax) != Double.doubleToLongBits(other.income_tax))
			return false;
		return true;
	}

}
