package com;

import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class EmployeeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter total no of employees");
		int total = 0;
		try {
			total = scan.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Wrong input");
			System.exit(0);
		}
		EmployeeVo[] employees = new EmployeeVo[total];
		EmployeeBo cal_income_tax = new EmployeeBo();
		try {
			for (int i = 0; i < total; i++) {

				System.out.println("Enter EmpID, EmpName and EmpAnnualIncome");
				int empid = scan.nextInt();
				String empname = scan.next();
				double annual_income = scan.nextDouble();
				scan.nextLine();
				employees[i] = new EmployeeVo(empid, empname, annual_income, 0);
				cal_income_tax.calincomeTax(employees[i]);
			}
		} catch (InputMismatchException e) {
			System.out.println("Wrong Inputs.");
			System.exit(0);
		}
		System.out.println("****Employees Details****");
		for (EmployeeVo e : employees) {
			System.out.println(e);
		}
		Arrays.sort(employees, new EmployeeSort());
		System.out.println("\nEmployees in Descending order of income tax");
		for (EmployeeVo e : employees) {
			System.out.println(e);
		}
	}

}
