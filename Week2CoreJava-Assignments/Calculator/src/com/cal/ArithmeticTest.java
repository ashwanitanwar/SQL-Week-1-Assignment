package com.cal;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ArithmeticTest {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter 2 numbers and choice such as 1. Add 2. Sub 3. Multiply 4. Divide");
		int a=0;
		int b=0;
		int c=0;
		try {
		a=scan.nextInt();
		b=scan.nextInt();
		c=scan.nextInt();
		} catch (InputMismatchException e)
		{
			System.out.println("Wrong inputs");
			System.exit(0);
		}
		
		Arithmetic[] array= {new Addition(a,b), new Subtraction(a, b), new Multiplication(a,b),new Division(a,b)};
		try {
		System.out.println("Result is: "+array[c-1].calculate());
		}catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Please choose between 1 to 4");
			System.exit(0);
		}
		catch(ArithmeticException e)
		{
			System.out.println("Invalid operations.Please check inputs.");
			System.exit(0);
		}
	}
}
