package com.cal;

import java.util.Scanner;

public abstract class Arithmetic {

	int num1;
	int num2;
	public Arithmetic(int num1, int num2) {
		this.num1 = num1;
		this.num2 = num2;
	}
	
	abstract public int calculate();
	
}
