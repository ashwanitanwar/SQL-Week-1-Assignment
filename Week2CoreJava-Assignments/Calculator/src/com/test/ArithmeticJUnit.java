package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.cal.Addition;
import com.cal.Division;
import com.cal.Multiplication;
import com.cal.Subtraction;

class ArithmeticJUnit {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@Test
	void testCalculateAdd() {
		int actual = new Addition(10, 50).calculate();

		assertEquals(60, actual);
	}

	@Test
	void testCalculateSub() {
		int actual = new Subtraction(50, 10).calculate();

		assertEquals(40, actual);
	}

	@Test
	void testCalculateMul() {
		int actual = new Multiplication(10, 50).calculate();

		assertEquals(500, actual);
	}

	@Test
	void testCalculateDiv() {
		int actual = new Division(50, 50).calculate();

		assertEquals(1, actual);
	}

}
